﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float speed = 20f;
    [SerializeField]
    LayerMask layerMask;
    [SerializeField]
    float sleepTime = 5f;
    float sleepTimer;
    BulletPool myPool;
    public BulletPool MyPool { set { myPool = value; } }

    void Update()
    {
        sleepTimer += Time.deltaTime;
        if (sleepTimer > sleepTime)
        {
            sleepTimer = 0;
            myPool.ReturnToPool(gameObject);
        }

        Vector3 prevPos = transform.position;
        transform.position += transform.forward * Time.deltaTime * speed;
        Vector3 dir = transform.position - prevPos;
        Ray ray = new Ray(prevPos, dir);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, dir.magnitude, layerMask))
        {
            Collide(hit.collider);
        }
    }

    void Collide(Collider col)
    {
        // pruefe ob col == enemy
        // wenn ja schicke enemy in pool zurueck
        // ausserdem explosion mit sound
        sleepTimer = 0;
        myPool.ReturnToPool(gameObject);
    }
}
