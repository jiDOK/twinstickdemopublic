﻿using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject wavePrefab;
    [SerializeField]
    AI ai;
    [SerializeField]
    PoolType poolType;

    void Start()
    {
        switch (poolType)
        {
            case PoolType.Hase:
                SpawnWave(wavePrefab, ai.hasenPool);
                break;
            case PoolType.DefaultEnemy:
                SpawnWave(wavePrefab, ai.defaultEnemyPool);
                break;
            case PoolType.Bla:
                break;
            default:
                break;
        }
    }

    public void SpawnWave(GameObject wave, EnemyPool pool)
    {
        int enemyCount = wavePrefab.transform.childCount;
        List<Vector3> enemyPositions = new List<Vector3>(enemyCount);

        foreach (Transform enemyPos in wavePrefab.transform)
        {
            enemyPositions.Add(enemyPos.position);
        }

        ai.Spawn(enemyPositions, pool);
    }
}

public enum PoolType
{
    Hase, DefaultEnemy, Bla
}
