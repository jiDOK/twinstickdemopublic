﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public const string level01name = "Level01";

    void OnEnable()
    {
        MenuEvents.Instance.StartPlaying.AddListener(ChangeScene);
    }

    void OnDisable()
    {
        MenuEvents.Instance?.StartPlaying.RemoveListener(ChangeScene);

        if (MenuEvents.Instance != null)
        {
            MenuEvents.Instance.StartPlaying.RemoveListener(ChangeScene);
        }
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(level01name);
    }

    public void ChangeScene(string sceneName)
    {
    }
}
