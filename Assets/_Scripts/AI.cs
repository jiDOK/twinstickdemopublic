﻿using UnityEngine;
using System.Collections.Generic;

public class AI : MonoBehaviour
{
    [SerializeField]
    GameObject defaultEnemy;
    [SerializeField]
    GameObject hase;
    public EnemyPool defaultEnemyPool;
    public EnemyPool hasenPool;
    public List<EnemyVehicle> enemyVehicles = new List<EnemyVehicle>(32);

    void Awake()
    {
        defaultEnemyPool = new EnemyPool(defaultEnemy);
        hasenPool = new EnemyPool(hase);
        defaultEnemyPool.InitializePool();
    }

    void Update()
    {
        for (int i = 0; i < enemyVehicles.Count; i++)
        {
            //getcomponent in der update ist zu performance-intensiv
            //enemyVehicles[i].GetComponent<SteeringBehaviour>().Steer();
            EnemyVehicle eV = enemyVehicles[i];
            for (int j = 0; j < eV.steeringBehaviours.Length; j++)
            {
                eV.steeringBehaviours[j].Steer();
            }
            eV.UpdateVehicle();
        }
    }

    public void Spawn(List<Vector3> positions, EnemyPool pool)
    {
        for (int i = 0; i < positions.Count; i++)
        {
            //Instantiate<GameObject>(defaultEnemy, positions[i], Quaternion.identity);

            // uebersichtliche version:
            //GameObject enemyObject = defaultEnemyPool.GetNext(positions[i]);
            //EnemyVehicle currentVehicle = enemyObject.GetComponent<EnemyVehicle>();
            //enemyVehicles.Add(currentVehicle);
            //schnelle version
            enemyVehicles.Add(pool.GetNext(positions[i]).GetComponent<EnemyVehicle>());
        }
    }
}

public class EnemyPool
{
    GameObject prefab;
    public Stack<GameObject> enemies = new Stack<GameObject>(16); 

    public EnemyPool(GameObject prefab)
    {
        this.prefab = prefab;
    }

    public void InitializePool()
    {
        for (int i = 0; i < 16; i++)
        {
            GameObject enemy = GameObject.Instantiate(prefab);
            enemies.Push(enemy);
        }
    }

    public GameObject GetNext(Vector3 atPosition)
    {
        if(enemies.Count == 0)
        {
            return null;
        }
        else
        {
            GameObject enemy = enemies.Pop();
            enemy.transform.position = atPosition;
            enemy.SetActive(true);
            return enemy;
        }
    }

    public void ReturnToPool(GameObject enemy)
    {
        enemy.SetActive(false);
        enemy.transform.position = Vector3.zero;
        enemies.Push(enemy);
    }
}
