﻿using UnityEngine;
using UnityEngine.Events;

public class MenuEvents : MonoBehaviour
{
    public enum TestEnum { Eins, Zwei, Three, Quattro }

    public UnityEvent StartPlaying = new UnityEvent();
    public UnityEvent<ActiveMenu> ChangeMenu = new ActiveMenuEvent();
    public UnityEvent<float> ChangeVolume = new FloatEvent();
    public UnityEvent PrintVolToUnitVal = new UnityEvent();
    public UnityEvent PrevTrack = new UnityEvent();
    public UnityEvent NextTrack = new UnityEvent();

    private static MenuEvents instance;
    public static MenuEvents Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<MenuEvents>();
            }
            return instance;
        }
        private set { instance = value; }
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void ToMainMenuButton()
    {
        ChangeMenu.Invoke(ActiveMenu.Main);
    }
    

    public void ToOptionsMenuButton()
    {
        ChangeMenu.Invoke(ActiveMenu.Options);
    }

    public void ToPlayButton()
    {
        StartPlaying.Invoke();
    }

    public void VolumeSlider(float val)
    {
        ChangeVolume.Invoke(val);
    }

    public void VolToUnitValButton()
    {
        PrintVolToUnitVal.Invoke();
    }

    public void NextTrackButton()
    {
        NextTrack.Invoke();
    }

    public void PrevTrackButton()
    {
        PrevTrack.Invoke();
    }
}

public enum ActiveMenu
{
    Main,
    Options,
}

[System.Serializable]
public class ActiveMenuEvent : UnityEvent<ActiveMenu>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}